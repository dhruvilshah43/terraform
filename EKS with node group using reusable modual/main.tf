provider "aws" {
  region = var.aws_region

}

# --------------------------------------
#                IAM Role
# --------------------------------------

module "iam" {
  source = "./modules/iam"
  #eks-role-policy = var.aws_iam_role_policy_attachment
}


# --------------------------------------
#                  VPC
# --------------------------------------
module "vpc" {
  source = "./modules/vpc"
  vpc_cidr = var.vpc_cidr
  public_subnet_cidr = var.public_subnet_cidr
  private_subnet_cidr = var.private_subnet_cidr
  az_1 = var.az_1
  az_2 = var.az_2
  env = var.env
}
# --------------------------------------
#               EKS Cluster
# --------------------------------------

module "eks_cluster" {
  source = "./modules/eks_cluster"
  role_arn = module.iam.EKSClusterRole
  public_subnets_id = module.vpc.vpc_public_subnet_id
  private_subnets_id = module.vpc.vpc_private_subnet_id
  security_group_id = module.vpc.vpc_security_group_id
  node_role_arn = module.iam.NodeGroupRole

  depends_on = [
    module.vpc 
  ]
}