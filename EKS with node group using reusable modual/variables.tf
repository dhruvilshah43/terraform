variable "aws_region" {
  default     = "ap-south-1"
  description = "aws region where our resources going to create choose"
  #replace the region as suits for your requirement
}


variable "env" {
    description = "Project environment.. "
    default = "prod"
}

variable "vpc_cidr" {
  description = "VPC CIDR.."
  type = string
  default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  description = "VPC Subnet A  CIDR"
  type = string
  default = "10.0.16.0/24"
}

variable "private_subnet_cidr" {
  description = "VPC Subnet B  CIDR"
  type = string
  default = "10.0.0.0/24"
} 

variable "az_1" {
  default     = "ap-south-1b"
  description = "Your AZ1, use AWS CLI to find your account specific"
}

variable "az_2" {
  default     = "ap-south-1c"
  description = "Your AZ2, use AWS CLI to find your account specific"
}



variable "cluster_config" {
  type = object({
    name    = string
    version = string
  })
  default = {
    name    = "eks-cluster"
    version = "1.22"
  }
}